package day8;

public class Task1SB {
    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();

        StringBuilder newString = new StringBuilder();

        for (int i = 0; i <= 20000; i++) {
            newString = newString.append(i + " ");
        }

        System.out.println(newString);

        long stopTime = System.currentTimeMillis();

        System.out.println("Время: " + (stopTime - startTime));

    }
}
