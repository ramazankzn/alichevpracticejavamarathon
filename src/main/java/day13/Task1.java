package day13;

public class Task1 {
    public static void main(String[] args) {
        User user1 = new User("rustickkk");
        User user2 = new User("ramazankzn");
        User user3 = new User("alishev");

        user1.sendMessage(user2, "Высылаю день 13");
        user1.sendMessage(user2, "*День13*");
        user2.sendMessage(user1, "Спасибо");
        user2.sendMessage(user1, "Делаю");
        user2.sendMessage(user1, "Жду день 14");
        user3.sendMessage(user1, "Зачем ты раздаешь мои курсы?");
        user3.sendMessage(user1, "Я ведь их с Сергеем делал");
        user3.sendMessage(user1, "Пусть " + user2.getUsername() + " тоже платит.");
        user1.sendMessage(user3, "Во-первых он мой друг");
        user1.sendMessage(user3, "Во-вторых информация должны быть свободной");
        user1.sendMessage(user3, "В-третьих он не хочет платить");
        user3.sendMessage(user1, "Понял :(");

        MessageDatabase.showDialog(user1, user3);
        System.out.println(MessageDatabase.getMessages().get(5).getDate());


    }
}
