package day13;

import java.util.ArrayList;
import java.util.List;

public class User {
    private final String username;
    private List<User> subscriptions;

    public String getUsername() {
        return username;
    }

    public List<User> getSubscriptions() {
        return subscriptions;
    }

    public User(String username) {
        this.username = username;
        this.subscriptions = new ArrayList<>();
    }

    public boolean isSubscribed(User user) {
        return subscriptions.contains(user);
    }

    public void subscribe(User user) {
        getSubscriptions().add(user);
    }

    public boolean isFriend(User user) {
        return user.isSubscribed(this) && this.isSubscribed(user);
    }

    public void sendMessage(User user, String text) {
        MessageDatabase.addNewMessage(this, user, text);
    }

    @Override
    public String toString() {
        return username;
    }
}
