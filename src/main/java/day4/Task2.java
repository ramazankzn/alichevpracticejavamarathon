package day4;

import java.util.Random;

//Создать новый массив размера 100 и заполнить его случайными числами из диапазона от 0 до 10000.
//Затем, используя цикл for each вывести в консоль:
//наибольший элемент массива
//наименьший элемент массива
//количество элементов массива, оканчивающихся на 0
//сумму элементов массива, оканчивающихся на 0
//
//Использовать сортировку запрещено.

public class Task2 {
    public static void main(String[] args) {
        int[] massive = new int[100];
        Random random = new Random();
        int max = 0;
        int min = Integer.MAX_VALUE;
        int summEndsZero = 0;
        int countEndsZero = 0;

        for (int i = 0; i < massive.length; i++) {
            massive[i] = random.nextInt(1000);
        }

        for (int number : massive
        ) {
            if (number > max) max = number;
            if (number < min) min = number;
            if (number % 10 == 0) {
                countEndsZero++;
                summEndsZero += number;
            }
        }


        System.out.println(max + " " + min + " " + countEndsZero + " " + summEndsZero);


    }
}
