package day4;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

// С клавиатуры вводится число n - размер массива. Необходимо создать массив указанного размера и заполнить его случайными числами от 0 до 10 (включительно). Используя цикл for each вывести содержимое массива в консоль, а также вывести в консоль информацию о:
//а) Длине массива
//б) Количестве чисел больше 8
//в) Количестве чисел равных 1
//г) Количестве четных чисел
//д) Количестве нечетных чисел
//е) Сумме всех элементов массива

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int higherEith = 0;
        int one = 0;
        int chet = 0;
        int nechet = 0;
        int summ = 0;
        int massiveSize = scanner.nextInt();
        int[] x = new int[massiveSize];

        for (int i = 0; i < x.length; i++) {
            x[i] = random.nextInt(11);
        }

        for (int i : x
        ) {
            if (i > 8) higherEith++;
            if (i == 1) one++;
            if (i % 2 == 0) chet++;
            else nechet++;
            summ += i;
        }

        System.out.println(Arrays.toString(x));
        System.out.println("Длина массива: " + x.length);
        System.out.println("Количество чисел больше 8: " + higherEith);
        System.out.println("Количество чисел равных 1: " + one);
        System.out.println("Количество четных чисел: " + chet);
        System.out.println("Количество нечетных чисел: " + nechet);
        System.out.println("Сумма всех элементов массива: " + summ);

    }
}
