package day4;

import java.util.Random;

//Создать новый массив размера 100 и заполнить его случайными числами из диапазона от 0 до 10000.
//Найти максимум среди сумм трех соседних элементов.
// Для найденной тройки с максимальной суммой выведите значение суммы и индекс первого элемента тройки.

public class Task4 {
    public static void main(String[] args) {


        // test array
        // int[] array = new int[] {1, 456, 1025, 65, 954, 5789, 4, 8742, 1040, 3254};
        int[] array = new int[100];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            // array[i] = random.nextInt(10000);
        }

        int maxSummOfThree = 0;
        int index = 0;

        for (int i = 0; i < array.length - 3; i++) {
            if (array[i] + array[i + 1] + array[i + 2] > maxSummOfThree) {
                maxSummOfThree = array[i] + array[i + 1] + array[i + 2];
                index = i;
            }
        }
        System.out.println(maxSummOfThree);
        System.out.println(index);

    }
}
