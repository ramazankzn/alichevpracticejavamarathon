package day18;

public class Tree {
    private Node root;

    public Tree(Node node) {
        this.root = node;
    }


    public void addNode(Node rootNode, int value) {
        if (value < rootNode.getValue()) {
            if (rootNode.getLeft() == null) {
                rootNode.setLeft(new Node(value));
            } else {
                addNode(rootNode.getLeft(), value);
            }
        } else {
            if (rootNode.getRight() == null) {
                rootNode.setRight(new Node(value));
            } else {
                addNode(rootNode.getRight(), value);
            }
        }
    }


    public void dfs(Node node) {
        if (node != null) {
            dfs(node.getLeft());
            System.out.print(node.getValue() + " ");
            dfs(node.getRight());
        }

    }
}
