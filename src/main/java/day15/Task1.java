package day15;
//Вам дан файл с информацией об остатках обуви на складе
//(src/main/resources/shoes.csv). Это пример реальной выгрузки остатков из 1С
//в csv файл (формат файла с разделителями, в качестве разделителя использован
//символ “;”). В этом файле содержится информация о названии модели обуви, ее
//размер и оставшееся на складе количество.
//До преобразования в csv это была таблица с тремя колонками:
//Название Размер Кол-во
//Ботинки HS РАН-Р 400 чер. ЗП 45 4
//Ботинки PANDA САНИТАРИ 3916 S1 SRC бел. 37 1
//...
//Необходимо сформировать новый файл
//(src/main/resources/missing_shoes.txt) с информацией о моделях и
//размерах обуви, которой нет на складе (количество = 0). Для этого реализуйте
//программу, которая принимает на вход csv файл и создает новый txt файл
//следующего содержания (должно получиться 11 строк):
//Ботинки СВАРЩИК ут М.1398 ЗП, 40, 0
//Ботинки СВАРЩИК ут М.1398 ЗП, 45, 0
//Ботинки ТОФФ БЕРКУТ И с выс.берцами ут ч, 38, 0
//...
//Возникающие исключения обрабатывать (try-catch), а не пробрасывать в сигнатуре
//метода (throws).
//Учтите ситуацию, когда в файле может быть содержимое отличное от ожидаемого. В
//нашем случае файл один и он корректен, но гипотетически может быть подан пустой
//файл или кол-во столбцов отличаться от трех.

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        File inputFile = new File("src/main/resources/shoes.csv");
        File outputFile = new File("src/main/resources/shoes.txt");
        try {
            PrintWriter printWriter = new PrintWriter(outputFile);
            printWriter.println(findWithZeroCount(fileToObj(inputFile)));
            printWriter.close();
            System.out.println("Файл записан");
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        }

    }

    public static @NotNull List<Shoes> fileToObj(File file) {
        List<Shoes> shoesList = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            if (file.length() != 0) {
                while (scanner.hasNext()) {
                    String[] strings = scanner.nextLine().split(";");
                    String shoesName = strings[0];
                    int size = Integer.parseInt(strings[1]);
                    int count = Integer.parseInt(strings[2]);
                    Shoes shoes = new Shoes(shoesName, size, count);
                    shoesList.add(shoes);
                }
            } else throw new IllegalArgumentException();
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (RuntimeException e) {
            System.out.println("Некорректный входной файл");
            e.printStackTrace();
            System.exit(1);
        }
        return shoesList;
    }

    private static List<Shoes> findWithZeroCount(@NotNull List<Shoes> list) {
        if (list.isEmpty()) {
            throw new NullPointerException();
        }
        List<Shoes> withZeroCount = new ArrayList<>();
        int withZeroCountLines = 0;
        for (Shoes oneShoes : list
        ) {
            if (oneShoes.getCount() == 0) {
                withZeroCount.add(oneShoes);
                withZeroCountLines++;
            }
        }
        System.out.println("Элементов в списке: " + withZeroCountLines);
        return withZeroCount;
    }
}
