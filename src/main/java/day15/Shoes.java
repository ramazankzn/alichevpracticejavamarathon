package day15;

public class Shoes {
    private String name;
    private int size;
    private int count;
    static final int MIN_SIZE = 30;
    static final int MAX_SIZE = 500;
    static final int MIN_COUNT = 0;

    public Shoes(String name, int size, int count) {
        this.setName(name);
        this.setSize(size);
        this.setCount(count);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if (size > MIN_SIZE && size < MAX_SIZE) {
            this.size = size;
        } else throw new IllegalArgumentException("Некорректный размер обуви!");
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        if (count >= MIN_COUNT) {
            this.count = count;
        } else throw new IllegalArgumentException("Количество не может быть отрицательным!");
    }

    @Override
    public String toString() {
        return "\n" + "Shoes{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", count=" + count +
                '}';
    }
}
