package day5;
//Создать класс Мотоцикл (англ. Motorbike) с полями "Модель", "Цвет", "Год
//выпуска”. Создать экземпляр класса Мотоцикл, используя конструктор (зет методы не
//использовать). Необходимо придерживаться принципов инкапсуляции и использовать
//ключевое слово тн =. Вывести в консоль значение каждого из полей, используя ее
//методы.


public class Task2 {
    public static void main(String[] args) {
        Motorbike bike1 = new Motorbike("CoolModel", "Green", 1992);

        System.out.println(bike1.getModel() + " " + bike1.getColour() + " " + bike1.getYear());

    }
}
