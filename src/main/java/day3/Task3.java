package day3;
// Реализовать программу, которая 5 раз запрашивает от пользователя два числа - делимое и делитель.
// Для этих двух чисел программа рассчитывает результат деления и выводит его в консоль. Если пользователь вводит 0 в качестве делителя,
// вместо того, чтобы останавливать работу цикла принудительно, мы пропускаем итерацию и выводим в консоль сообщение “Деление на 0”.
//Ключевое слово else или два if использовать в этой программе нельзя. В решении обязательно использовать оператор continue.

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int i = 0;
        Scanner scanner = new Scanner(System.in);
        double delimoe = 0;
        double delitel = 0;

        while (i < 5) {
            delimoe = scanner.nextDouble();
            delitel = scanner.nextDouble();
            if (delitel == 0) {
                System.out.println("Деление на 0");
                i++;
                continue;
            }

            System.out.println(delimoe / delitel);
            i++;

        }
    }
}
