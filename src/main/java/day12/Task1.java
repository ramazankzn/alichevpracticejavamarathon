package day12;
//Создать список строк, добавить в него 5 марок автомобилей, вывести список в
//консоль. Добавить в середину еще 1 автомобиль, удалить самый первый автомобиль
//из списка. Вывести список в консоль.

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>(Arrays.asList("Audi", "BMW", "Toyota", "VAZ", "Opel"));
        System.out.println(arrayList);
        arrayList.add(3, "Honda");
        arrayList.remove(0);
        System.out.println(arrayList);

    }
}
