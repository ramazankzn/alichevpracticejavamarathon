package day12.task4;

import java.util.ArrayList;
import java.util.List;

public class MusicBand {
    String name;
    int year;
    List<String> members;

    public MusicBand(String name, int year, List<String> members) {
        this.name = name;
        this.year = year;
        this.members = members;
    }

    public static void transferMembers(MusicBand musicBandFromTransfer, MusicBand musicBandForTransfer) {
        musicBandForTransfer.members.addAll(musicBandFromTransfer.members);
        musicBandFromTransfer.members.clear();
    }

    @Override
    public String toString() {
        return "MusicBand{" + "name='" + name + '\'' + ", year=" + year + '}';
    }


    public void printMembers() {
        for (Object member : MusicBand.this.members) {
            System.out.println(member);
        }
    }

    public List<String> getMembers() {
        return this.members;
    }
}
