package day12.task4;
//Скопировать MusicBand из прошлого задания и доработать таким образом, чтобы в
//группу можно было добавлять и удалять участников. Под участником понимается
//строка (String) с именем и фамилией. Реализовать статический метод слияния групп
//(в классе MusicBand), т.е. все участники группы А переходят в группу B. Название
//метода: transferMembers. Этот метод принимает в качестве аргументов два
//экземпляра класса MusicBand. В классе MusicBand, реализовать метод
//printMembers(), печатающий список участников в консоль и метод getMembers(),
//возвращающий список участников.
//Проверить состав групп после слияния.

import java.util.ArrayList;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<String> upSurdeMembers = new ArrayList<>();
        upSurdeMembers.add("Sergey Yaushev");
        upSurdeMembers.add("Rufat Tabaev");
        upSurdeMembers.add("Aleksei Barmin");
        MusicBand upSurde = new MusicBand("UpSurde", 2022, upSurdeMembers);

        List<String> hasNoGroup = new ArrayList<>();
        hasNoGroup.add("Ramazan Khakimov");
        MusicBand noGroup = new MusicBand("Don't Have a group", 2020, hasNoGroup);

        MusicBand.transferMembers(noGroup, upSurde);

        upSurde.printMembers();
        System.out.println();
        noGroup.printMembers();


    }
}
