package day12;
//Создать новый список, заполнить его четными числами от 0 до 30 и от 300 до 350.
//Для заполнения списка реализуйте метод, который принимает числа от, до, и ссылку
//на список. Вывести список.

import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 350; i++) {
            if (i % 2 == 0 && (i < 30 || i > 299)) {
                arrayList.add(i);
            }
        }
        System.out.println(arrayList);

    }
}
