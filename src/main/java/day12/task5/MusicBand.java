package day12.task5;

import java.util.List;

public class MusicBand {
    String name;
    int year;
    List<MusicArtist> members;

    public MusicBand(String name, int year, List<MusicArtist> members) {
        this.name = name;
        this.year = year;
        this.members = members;
    }

    public static void transferMembers(MusicBand musicBandFromTransfer, MusicBand musicBandForTransfer) {
        musicBandForTransfer.members.addAll(musicBandFromTransfer.members);
        musicBandFromTransfer.members.clear();
    }

    @Override
    public String toString() {
        return "MusicBand{" +
                "name='" + name + '\'' +
                ", year=" + year +
                '}';
    }


    public void printMembers() {
        for (Object member : MusicBand.this.members
        ) {
            System.out.println(member);
        }
    }

    public List<MusicArtist> getMembers() {

        return this.members;
    }
}
