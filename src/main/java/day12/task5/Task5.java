package day12.task5;
//Скопировать MusicBand из прошлого задания и доработать - теперь у участника
//музыкальной группы есть не только имя, но и возраст. Соответственно, теперь под
//участником понимается не строка, а объект класса MusicArtist. Необходимо
//реализовать класс MusicArtist и доработать класс MusicBand (создать копию
//класса) таким образом, чтобы участники были - объекты класса MusicArtist. После
//этого, надо сделать то же самое, что и требовалось в 4 задании - слить две группы и
//проверить состав групп после слияния. Методы для слияния и для вывода участников
//в консоль необходимо тоже переработать, чтобы они работали с объектами класса
//MusicArtist.

import java.util.ArrayList;
import java.util.List;

public class Task5 {
    public static void main(String[] args) {
        List<MusicArtist> upSurdeMembers = new ArrayList<>();
        upSurdeMembers.add(new MusicArtist("Sergey Yaushev", 29));
        upSurdeMembers.add(new MusicArtist("Rufat Tabaev", 30));
        upSurdeMembers.add(new MusicArtist("Aleksei Barmin", 30));
        MusicBand upSurde = new MusicBand("UpSurde", 2022, upSurdeMembers);

        List<MusicArtist> hasNoGroup = new ArrayList<>();
        hasNoGroup.add(new MusicArtist("Ramazan Khakimov", 30));
        MusicBand noGroup = new MusicBand("Don't Have a group", 2020, hasNoGroup);

        MusicBand.transferMembers(noGroup, upSurde);

        upSurde.printMembers();
        System.out.println();
        noGroup.printMembers();

    }
}
