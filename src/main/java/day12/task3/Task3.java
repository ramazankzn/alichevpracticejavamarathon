package day12.task3;
//Создать класс Музыкальная Группа (англ. MusicBand) с полями name и year
//(название музыкальной группы и год основания). Создать 10 или более экземпляров
//класса MusicBand , добавить их в список (выбирайте такие музыкальные группы,
//которые были созданы как до 2000 года, так и после, жанр не важен). Перемешать
//список. Создать статический метод в классе Task3:
//public static List<MusicBand> groupsAfter2000(List<MusicBand>
//bands)
//Этот метод принимает список групп в качестве аргумента и возвращает новый список,
//состоящий из групп, основанных после 2000 года. Вызвать метод
//groupsAfter2000(List<MusicBand> bands) в методе main() на вашем списке
//из 10 групп. Вывести в консоль оба списка (оригинальный и с группами, основанными
//после 2000 года).

import java.util.*;

public class Task3 {
    public static void main(String[] args) {
        MusicBand musicBand1 = new MusicBand("Red Hot Chili Peppers", 1983);
        MusicBand musicBand2 = new MusicBand("UpSurde", 2022);
        MusicBand musicBand3 = new MusicBand("The Beatles", 1960);
        MusicBand musicBand4 = new MusicBand("Дайте танк (!)", 2007);
        MusicBand musicBand5 = new MusicBand("ABBA", 1978);
        MusicBand musicBand6 = new MusicBand("Coldplay", 1996);
        MusicBand musicBand7 = new MusicBand("Maroon 5", 1994);
        MusicBand musicBand8 = new MusicBand("System Of A Down", 1994);
        MusicBand musicBand9 = new MusicBand("The Doors", 1965);
        MusicBand musicBand10 = new MusicBand("The Jimi Hendrix Experience", 1966);

        List<MusicBand> arrayList = new ArrayList<>(Arrays.asList(musicBand1, musicBand2, musicBand3, musicBand4, musicBand5, musicBand6, musicBand7, musicBand8, musicBand9, musicBand10));
        System.out.println(arrayList);
        Collections.shuffle(arrayList);
        System.out.println(arrayList);

        System.out.println(groupsAfter2000(arrayList));

    }

    public static List<MusicBand> groupsAfter2000(List<MusicBand> bands) {
        List<MusicBand> groupsAfter2000 = new ArrayList<>();
        for (MusicBand band : bands
        ) {
            if (band.year > 2000) {
                groupsAfter2000.add(band);
            }
        }
        return groupsAfter2000;
    }
}
