package day2;
// Реализовать программу №2, используя цикл while.

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int a = 0;
        int b = 0;

        Scanner scanner = new Scanner(System.in);

        a = scanner.nextInt();
        b = scanner.nextInt();

        if (a >= b) System.out.println("Некорректный ввод");
        a += 1;

        while (a < b) {
            if (a % 5 == 0 && a % 10 != 0) {
                System.out.print(a + " ");
            }
            a++;
        }

    }
}
