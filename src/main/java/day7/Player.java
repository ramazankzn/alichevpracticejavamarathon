package day7;

public class Player {
    private int stamina;
    static final int MAX_STAMINA = 100;
    static final int MIN_STAMINA = 0;
    private static int countPlayers = 0;

    public Player(int stamina) {
        this.stamina = stamina;
        if (countPlayers < 6) countPlayers++;
    }

    public int getStamina() {
        return stamina;
    }

    public static int getCountPlayers() {
        return countPlayers;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public void run() {
        if (getStamina() == 0) return;
        setStamina(getStamina() - 1);
        if (getStamina() == 0) countPlayers--;
    }

    public static void info() {
        if (getCountPlayers() < 6)
            System.out.println("Команды неполные. На поле еще есть " + (6 - getCountPlayers()) + " свободных мест");
        else System.out.println("На поле нет свободных мест");
    }


}
