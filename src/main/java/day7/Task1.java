package day7;
//Для этого задания скопируйте класс Самолет из предыдущего дня в пакет текущего дня
//
//В классе Самолет реализовать статический метод compareAirplanes, который е
//качестве аргументов принимает два объекта класса Airplane (два самолета) и
//выводит сообщение в консоль о том, какой из самолетов длиннее.

public class Task1 {
    public static void main(String[] args) {
        Airplane airplaneBoeing = new Airplane("Boeing", 1992, 100, 10000);
        Airplane airplaneAirbus = new Airplane("Airbus", 2000, 150, 15000);

        Airplane.compareAirplanes(airplaneBoeing, airplaneAirbus);

    }
}