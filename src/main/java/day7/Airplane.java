package day7;

public class Airplane {
    String manufacturer;
    int year;
    int length;
    int weidht;
    int fuel = 0;

    public void info() {
        System.out.println("Изготовитель: " + manufacturer
                + ", год выпуска: " + year
                + ", длина: " + length
                + ", вес: " + weidht
                + ", количество топлива в баке: " + fuel);
    }

    public int getLength() {
        return length;
    }

    public Airplane(String manufacturer, int year, int length, int weidht) {
        this.manufacturer = manufacturer;
        this.year = year;
        this.length = length;
        this.weidht = weidht;
    }

    public void fillUp(int n) {
        setFuel(getFuel() + n);
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setWeidht(int weidht) {
        this.weidht = weidht;
    }

    public static void compareAirplanes(Airplane airplane1, Airplane airplane2) {
        if (airplane1.getLength() > airplane2.getLength()) System.out.println("Первый самолёт длиннее");
        else if (airplane2.getLength() > airplane1.getLength()) System.out.println("Второй самолет длиннее");
        else System.out.println("Длина самолетов равна");
    }
}
