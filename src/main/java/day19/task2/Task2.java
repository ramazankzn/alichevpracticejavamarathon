package day19.task2;
//В папке resources находится файл taxi_cars.txt. Этот файл содержит в себе
//данные о местоположении 500 машин такси (каждая машина такси находится на новой
//строке).
//Формат данных в файле следующий:
//id_машины координата_по_x координата_по_y
//Каждая строка в файле содержит 3 целых числа, разделенных пробелами —
//уникальный идентификатор машины такси, координата машины по оси X и координата
//машины такси по оси Y. Значения каждой из координат находятся в диапазоне от 0 до 99.
// После того, как все машины будут лежать в объекте HashMap, вам необходимо запросить от
//пользователя 4 числа - координаты квадрата (x1, y1, x2, y2). Первые два числа —
//координаты первой точки квадрата, вторые два числа — координаты второй точки квадрата
//Для квадрата, введенного пользователем, вам необходимо, используя
//сформированный объект HashMap<Integer, Point>, определить, какие машины
//попали внутрь этого квадрата. Вам необходимо вывести в консоль идентификаторы
//машин, которые находятся внутри квадрата и общее количество машин в квадрате.
//Машины, лежащие на грани квадрата, не учитываются.
//Алгоритм должен работать для любого корректно введенного квадрата.

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * @author Neil Alishev
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите x1: ");
        int x1 = scanner.nextInt();
        System.out.println("Введите y1: ");
        int y1 = scanner.nextInt();
        System.out.println("Введите x2: ");
        int x2 = scanner.nextInt();
        System.out.println("Введите y2: ");
        int y2 = scanner.nextInt();
        carsInSquare(x1, y1, x2, y2);

    }

    private static HashMap<Integer, Point> readCarsFromFile() {
        File file = new File("src/main/resources/taxi_cars.txt");
        HashMap<Integer, Point> cars = new HashMap<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String[] carString = scanner.nextLine().split(" ");
                cars.put(Integer.parseInt(carString[0]), new Point(Integer.parseInt(carString[1]), Integer.parseInt(carString[2])));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return cars;
    }

    private static void carsInSquare(int x1, int y1, int x2, int y2) {
        List<Integer> carsInSquare = new ArrayList<>();
        HashMap<Integer, Point> cars = readCarsFromFile();
        for (Map.Entry<Integer, Point> car : cars.entrySet()
        ) {
            int x = car.getValue().getX();
            int y = car.getValue().getY();
            if (x > x1 && y > y1 && x < x2 && y < y2) {
                carsInSquare.add(car.getKey());
            }
        }
        System.out.println("Машины в квадрате: " + carsInSquare);
        System.out.println("Всего машин в квадрате: " + carsInSquare.size());
    }
}

