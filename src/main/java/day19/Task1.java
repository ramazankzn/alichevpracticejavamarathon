package day19;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) throws FileNotFoundException {
        HashMap<String, Integer> words = new HashMap<>();
        File book = new File("src/main/resources/dushi.txt");
        Scanner scanner = new Scanner(book);
        scanner.useDelimiter("[.,:;()?!\"\\s–]+");
        while (scanner.hasNext()) {
            int count = 1;
            String word = scanner.next();
            if (words.containsKey(word)) {
                words.put(word, words.get(word) + 1);
            } else words.put(word, count);
        }


        words.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(System.out::println);
    }
}
