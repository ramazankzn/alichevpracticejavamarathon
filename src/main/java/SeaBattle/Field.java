package SeaBattle;

import SeaBattle.ShipsAndCells.*;

import java.util.ArrayList;
import java.util.List;

public class Field {

    private static final int BATTLEFIELDSIZE = 10;
    private Cell[][] battlefield = new Cell[BATTLEFIELDSIZE][BATTLEFIELDSIZE];
    private List<Ship> ships = new ArrayList<>();

    public List<Ship> getShips() {
        return ships;
    }

    public Cell[][] getBattlefield() {
        return battlefield;
    }


    private void fillAura(Ship ship) {
        if (ship instanceof Ship1) {
            if (ship.getY1() == 0 && ship.getX1() == 0) {
                battlefield[ship.getY1()][ship.getX1() + 1] = new ShipAura(ship);

                battlefield[ship.getY1() + 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() + 1] = new ShipAura(ship);
            } else if (ship.getY1() == battlefield.length - 1 && ship.getX1() == battlefield.length - 1) {
                battlefield[ship.getY1()][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1() - 1] = new ShipAura(ship);
            } else if (ship.getY1() == battlefield.length - 1 && ship.getX1() == 0) {
                battlefield[ship.getY1() - 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() + 1] = new ShipAura(ship);
            } else if (ship.getY1() == 0 && ship.getX1() == battlefield.length - 1) {
                battlefield[ship.getY1()][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1()] = new ShipAura(ship);
            } else if (ship.getY1() == 0 && ship.getX1() != 0) {
                battlefield[ship.getY1()][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() + 1] = new ShipAura(ship);
            } else if (ship.getY1() != 0 && ship.getX1() == 0) {
                battlefield[ship.getY1() - 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1()] = new ShipAura(ship);
            } else if (ship.getY1() == battlefield.length - 1 && ship.getX1() != 0) {
                battlefield[ship.getY1() - 1][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() + 1] = new ShipAura(ship);
            } else if (ship.getY1() != 0 && ship.getX1() == battlefield.length - 1) {
                battlefield[ship.getY1() - 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1()] = new ShipAura(ship);
            } else {
                battlefield[ship.getY1() - 1][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() - 1][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1()][ship.getX1() + 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() - 1] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1()] = new ShipAura(ship);
                battlefield[ship.getY1() + 1][ship.getX1() + 1] = new ShipAura(ship);
            }

        }
    }

    protected void addShipToField(Ship ship) {
        if (!battlefield[ship.getY1()][ship.getX1()].isAuraOrShip()) {
            battlefield[ship.getY1()][ship.getX1()] = ship;
            fillAura(ship);
            ships.add(ship);
        } else {
            System.out.println("Некорректное расположение");
            throw new IllegalArgumentException();
        }
    }

    protected boolean checkShipOnCell(int x, int y) {
        return battlefield[y][x].isShip();
    }

    protected void generateEmptyBattlefield() {
        for (int i = 0; i < battlefield.length; i++) {
            for (int j = 0; j < battlefield[i].length; j++) {
                battlefield[i][j] = new EmptyCell();
            }
        }
    }

    protected void printBattlefield() {
        System.out.println("⓪ " + "①" + " ②" + " ③" + " ④" + " ⑤" + " ⑥" + " ⑦" + " ⑧" + " ⑨" + " ⑩");
        for (int i = 0; i < battlefield.length; i++) {
            System.out.print(printNumberSymbol(i + 1) + " ");
            for (int j = 0; j < battlefield[i].length; j++) {
                if (battlefield[i][j].isOpen()) {
                    System.out.print(battlefield[i][j] + " ");
                } else System.out.print(battlefield[i][j] + " ");
            }
            System.out.println();
        }
    }

    protected void printOpenBattlefield() {
        System.out.println("⓪ " + "①" + " ②" + " ③" + " ④" + " ⑤" + " ⑥" + " ⑦" + " ⑧" + " ⑨" + " ⑩");
        for (int i = 0; i < battlefield.length; i++) {
            System.out.print(printNumberSymbol(i + 1) + " ");

            for (int j = 0; j < battlefield[i].length; j++) {
                if (battlefield[i][j].isShip()) {
                    battlefield[i][j].setOpen();
                    System.out.print(battlefield[i][j] + " ");
                    battlefield[i][j].setClose();
                } else System.out.print(battlefield[i][j] + " ");

            }
            System.out.println();
        }
    }

    private char printNumberSymbol(int number) {
        char numberSymbol = '⓪';
        switch (number) {
            case 1:
                numberSymbol = '①';
                break;
            case 2:
                numberSymbol = '②';
                break;
            case 3:
                numberSymbol = '③';
                break;
            case 4:
                numberSymbol = '④';
                break;
            case 5:
                numberSymbol = '⑤';
                break;
            case 6:
                numberSymbol = '⑥';
                break;
            case 7:
                numberSymbol = '⑦';
                break;
            case 8:
                numberSymbol = '⑧';
                break;
            case 9:
                numberSymbol = '⑨';
                break;
            case 10:
                numberSymbol = '⑩';
                break;
        }
        return numberSymbol;
    }

    protected void scanShipCoordinates() {
    }


}
