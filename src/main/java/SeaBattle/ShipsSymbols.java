package SeaBattle;

public enum ShipsSymbols {
    SHIP("\uD83D\uDEE5"),
    EMPTY("⬜"),
    AURA("\uD83D\uDFE6"),
    MISS("\uD83D\uDFE5"),
    WOUNDED("\uD83D\uDD25"),
    KILLED("❌");

    private String kindOfShip;

    ShipsSymbols(String kindOfShip) {
        this.kindOfShip = kindOfShip;
    }

    public String getKindOfShip() {
        return kindOfShip;
    }
}
