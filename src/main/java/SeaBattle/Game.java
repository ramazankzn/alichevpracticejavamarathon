package SeaBattle;

import SeaBattle.ShipsAndCells.Ship;
import SeaBattle.ShipsAndCells.Ship1;
import SeaBattle.ShipsAndCells.ShipAura;

import java.util.Scanner;

public class Game {
    private Player player1, player2;
    private boolean isGamePlaying;

    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    protected void startGame() {
        isGamePlaying = true;
        Field player1Field = new Field();
        player1Field.generateEmptyBattlefield();
        Field player2Field = new Field();
        player2Field.generateEmptyBattlefield();
        placeShips(player1);
        placeShips(player2);
        while (isGamePlaying) {
            makeShoot(player1, player2);
            makeShoot(player2, player1);
        }
    }

    private boolean correctCoordinatesForShoot(int x, int y, Player playerBattlefieldCheckForOpen) {
        if ((x < 0 || x > 9) || (y < 0 || y > 9)) {
            System.out.println("Некорректные координаты. Необходимо вводить числа в формате x;y от 1 до 10 (Например: 2;3)");
            return false;
        }
        if (playerBattlefieldCheckForOpen.getField().getBattlefield()[y][x].isOpen()) {
            System.out.println("Нет смысла сюда стрелять.");
            return false;
        }
        return true;
    }

    private void makeShoot(Player shooter, Player defender) {
        int x, y;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вот поле " + defender.getName());
        defender.getField().printBattlefield();
        do {
            do {
                System.out.println("Игрок " + shooter.getName() + " введи координаты для выстрела в виде \"x;y:\"");
                String[] coordinates = scanner.nextLine().split(";");
                x = Integer.parseInt(coordinates[0]) - 1;
                y = Integer.parseInt(coordinates[1]) - 1;
            } while (!correctCoordinatesForShoot(x, y, defender));
            defender.getField().getBattlefield()[y][x].setOpen();
            if (defender.getField().checkShipOnCell(x, y)) {
                Ship ship = (Ship) defender.getField().getBattlefield()[y][x];
                System.out.println("Попал!");

                if (ship.getShipLength() == 0) {
                    ship.setDied();
                    defender.getField().getShips().remove(ship);
                    System.out.println("Убил!");
                    for (ShipAura aura : ship.getShipAura()
                    ) {
                        aura.setOpen();
                    }
                    if (defender.getField().getShips().isEmpty()) {
                        shooter.setWinner();
                        System.out.println("Теперь поле игрока " + defender + " выглядит так:");
                        defender.getField().printBattlefield();
                        System.out.println("Игрок " + shooter + " победил!");
                        System.out.println(defender +  " посмотри." + "Вот поля игрока: " + shooter);
                        shooter.getField().printOpenBattlefield();
                        isGamePlaying = false;
                        break;
                    }
                }
            }
            System.out.println("Теперь поле игрока " + defender + " выглядит так:");
            defender.getField().printBattlefield();
        }
        while (defender.getField().checkShipOnCell(x, y));
        if (!shooter.isWinner()) {
            System.out.println("Жаль, но это промах. Ход переходит к " + defender);
        }
    }

    private void placeShips(Player player) {
        System.out.println("Игрок " + player + " начинает располагать однопалубные корабли. Второй игрок, не подглядывай!");
        Scanner scanner = new Scanner(System.in);
        int x, y;

        for (int countOfShips = 1; countOfShips < 5; countOfShips++) {
            do {
                System.out.print("Введите x для расположения " + countOfShips + "-го корабля:");
                x = scanner.nextInt() - 1;
                System.out.print("Введите y для расположения " + countOfShips + "-го корабля:");
                y = scanner.nextInt() - 1;
            }
            while (!isCanPlaceShipHere(x, y, player));
            Ship ship = new Ship1(x, y);
            player.getField().addShipToField(ship);
        }
    }

    private boolean isCanPlaceShipHere(int x, int y, Player player) {
        if ((x < 0 || x > 9) || (y < 0 || y > 9)) {
            System.out.println("Некорректные координаты. Необходимо вводить числа в формате x;y от 1 до 10 (Например: 2;3)");
            return false;
        }
        if (player.getField().getBattlefield()[y][x].isAuraOrShip()) {
            System.out.println("На этом поле нельзя разместить корабль.");
            return false;
        }
        return true;
    }
}
