package SeaBattle;


public class Main {
    public static void main(String[] args) {
        Field player1Field = new Field();
        player1Field.generateEmptyBattlefield();
        Field player2Field = new Field();
        player2Field.generateEmptyBattlefield();
        Game game = new Game(new Player("Ram", player1Field), new Player("Adelya", player2Field));
        game.startGame();
    }
}
