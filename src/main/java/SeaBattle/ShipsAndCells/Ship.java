package SeaBattle.ShipsAndCells;

import SeaBattle.ShipsSymbols;

import java.util.ArrayList;
import java.util.List;

public abstract class Ship extends Cell {
    private int shipLength;

    private boolean isDied = false;
    private List<ShipAura> shipAura = new ArrayList<>();

    public int getShipLength() {
        return shipLength;
    }

    public void setShipLength(int shipLength) {
        this.shipLength = shipLength;
    }

    public List<ShipAura> getShipAura() {
        return shipAura;
    }

    public void setDied() {
        isDied = true;
    }

    public String toString() {
        if (isDied && isOpen()) return ShipsSymbols.KILLED.getKindOfShip();
        else if (isOpen() && !isDied) return ShipsSymbols.SHIP.getKindOfShip();
        return ShipsSymbols.EMPTY.getKindOfShip();
    }


    public abstract int getX1();

    public abstract int getY1();
}
