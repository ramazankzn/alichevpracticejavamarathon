package SeaBattle.ShipsAndCells;

import SeaBattle.ShipsSymbols;

public class EmptyCell extends Cell {


    @Override
    public String toString() {
        if (isOpen())
            return ShipsSymbols.AURA.getKindOfShip();
        return ShipsSymbols.EMPTY.getKindOfShip();
    }
}
