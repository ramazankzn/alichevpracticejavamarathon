package SeaBattle.ShipsAndCells;


public class Ship1 extends Ship {
    private int x1, y1;

    @Override
    public void setShipLength(int shipLength) {
        super.setShipLength(1);
    }

    public Ship1(int x1, int y1) {
        this.x1 = x1;
        this.y1 = y1;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }


}
