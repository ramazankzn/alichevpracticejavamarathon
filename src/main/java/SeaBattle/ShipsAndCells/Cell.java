package SeaBattle.ShipsAndCells;

public abstract class Cell {
    private boolean isOpen = false;

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen() {
        this.isOpen = true;
    }

    public void setClose() {
        this.isOpen = false;
    }


    public boolean isAuraOrShip() {
        return this instanceof Ship || this instanceof ShipAura;
    }

    public boolean isShip() {
        return this instanceof Ship;
    }
}
