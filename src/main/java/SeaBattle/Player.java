package SeaBattle;

public class Player {
    private String name;
    private Field field;
    private boolean isWinner = false;

    public boolean isWinner() {
        return isWinner;
    }

    public void setWinner() {
        isWinner = true;
    }

    public Field getField() {
        return field;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player(String name, Field field) {
        this.field = field;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
