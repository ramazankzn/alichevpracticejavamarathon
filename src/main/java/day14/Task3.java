package day14;
//Создать класс “Человек” с полями “имя” и “возраст”. Реализовать статический метод
//List<Person> parseFileToObjList(File file), который считывает
//содержимое того же файла people.txt, создает экземпляры класса “Человек” и
//возвращает список объектов. Получить данный список в методе main() и распечатать
//его в консоль.
//В случае, если файла не существует в папке проекта, в программе необходимо
//выбрасывать исключение и выводить в консоль сообщение “Файл не найден”. Помимо
//этого, если значение возраста отрицательно, необходимо выбрасывать исключение и
//выводить в консоль сообщение “Некорректный входной файл”.
//Пример ответа: [{name='Mike', year=24}, {name='John', year=19},
//{name='Anna', year=20}, {name='Miguel', year=5}]

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        File file = new File("src/main/java/day14/people");
        System.out.println(parseFileToObjList(file));

    }

    public static List<Person> parseFileToObjList(File file) {
        List<Person> personList = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String[] nameAndAge = scanner.nextLine().split(" ");
                String name = nameAndAge[0];
                int age = Integer.parseInt(nameAndAge[1]);
                if (age < 0) {
                    throw new AgeException();
                }
                Person person = new Person(name, age);
                personList.add(person);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (AgeException ageException) {
            System.out.println("Некорректный входной файл");
            return null;
        }
        return personList;
    }
}
