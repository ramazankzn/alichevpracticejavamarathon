package day14;
//Создать .txt файл в папке проекта, как показано в видео (урок 36, время 15:30).
//Заполнить его вручную десятью произвольными числами. Реализовать статический
//метод printSumDigits(File file), который считает сумму всех чисел в этом
//файле и выводит ее на экран.
//Если файла не существует в папке проекта, в программе необходимо выбрасывать
//исключение и выводить в консоль сообщение “Файл не найден”. Помимо этого, если
//чисел в файле меньше или больше 10, необходимо выбрасывать исключение и
//выводить в консоль сообщение “Некорректный входной файл”.
//Вызвать данный метод в методе main().

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        File file = new File("src/main/java/day14/Numbers for t1");
        printSumDigits(file);
    }

    public static void printSumDigits(File file) {
        try (Scanner fileReader = new Scanner(file)) {
            int sum = 0;
            int i = 0;
            while (fileReader.hasNext()) {
                sum += fileReader.nextInt();
                i++;
            }
            if (i != 10) {
                throw new Exception();
            }
            System.out.println(sum);
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (Exception e) {
            System.out.println("Некорректный входной файл");
        }

    }
}
