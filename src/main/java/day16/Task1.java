package day16;
//Реализовать программу, которая на вход принимает txt файл с целыми числами
//(можно использовать файл из задания 1 дня 14) и в качестве ответа выводит в консоль
//среднее арифметическое этих чисел.
//Ответ будет являться вещественным числом. В консоль необходимо вывести полную
//запись вещественного числа (со всеми знаками после запятой) и сокращенную запись
//(с 3 знаками после запятой).
//Детали реализации: В классе Task1 создать публичный статический метод
//printResult(File file), в котором реализовать вышеописанную логику. В методе
//main() класса Task1 вызвать метод printResult(File file), передав ему в
//качестве аргумента объект класса File (txt файл с целыми числами). Возникающие
//исключения обрабатывать внутри метода.

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        File file = new File("src/main/java/day16/numbers16.txt");
        printResult(file);

    }

    public static void printResult(File file) {
        try {
            Scanner scanner = new Scanner(file);
            List<Integer> numbers = new ArrayList<>();
            int sum = 0;
            while (scanner.hasNextInt()) {
                numbers.add(scanner.nextInt());
            }
            scanner.close();
            for (Integer number : numbers) {
                sum += number;
            }
            double average = (double) sum / (double) numbers.size();
            System.out.printf(average + " --> %.3f", average);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

