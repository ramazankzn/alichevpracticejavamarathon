package day16;
//Реализовать программу, записывающую числа разных типов в 2 файла.
//Файл 1 должен называться “file1.txt”, а Файл 2 должен называться “file2.txt”.
//Оба файла должны находиться в корне проекта. Создаваться файлы должны не
//вручную, а при запуске программы.
//Файл 1 должен содержать 1000 случайных чисел от 0 до 100.
//Файл 2 создается на основании Файла 1, но содержит числа вещественного типа
//данных.
//Метод заполнения Файла 2 следующий: для каждой группы из 20 целых чисел из
//Файла 1 рассчитывается их среднее арифметическое. Затем, полученное значение
//записывается в Файл 2. Таким образом в Файле 2 будет находиться 50 вещественных
//чисел (1000 / 20 = 50).
//После того, как Файл 2 будет сформирован, необходимо реализовать статический
//метод printResult(File file). Этот метод должен рассчитать сумму всех
//вещественных чисел из Файла 2 и вывести её в консоль, отбросив вещественную
//часть.

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) throws FileNotFoundException {

        File firstFile = new File("src/main/java/day16/file1.txt");
        File secondFile = new File("src/main/java/day16/file2.txt");
        fillFileWithRandomNumbers(firstFile);
        fillFileWithAverage(firstFile, secondFile);
        printResult(secondFile);

    }

    private static void fillFileWithRandomNumbers(File file) throws FileNotFoundException {
        PrintWriter printWriter1 = new PrintWriter(file);
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            printWriter1.println(random.nextInt(100));
        }
        printWriter1.close();
    }

    private static void fillFileWithAverage(File inputFile, File outputFile) throws FileNotFoundException {
        try (Scanner scanner = new Scanner(inputFile); PrintWriter printWriter = new PrintWriter(outputFile)) {

            List<Integer> numbersFromFile = new ArrayList<>();
            double averageOfTwenty = 0;
            while (scanner.hasNextInt()) {
                numbersFromFile.add(scanner.nextInt());
            }
            for (int i = 0; i <= 980; i = i + 20) {
                int sumOfTwenty = 0;
                for (int j = 0; j < 20; j++) {
                    sumOfTwenty += numbersFromFile.get(i + j);
                }
                averageOfTwenty = sumOfTwenty / 20.0;
                printWriter.println(averageOfTwenty);
            }
        }
    }


    public static void printResult(File file) {
        try {
            Scanner scanner = new Scanner(file);
            double sum = 0;
            String[] numbersString;
            while (scanner.hasNext()) {
                numbersString = scanner.nextLine().split(" ");
                for (String string : numbersString) {
                    sum += Double.parseDouble(string);
                }
            }
            scanner.close();

            int doubleToInt = (int) sum;
            System.out.println(doubleToInt);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
