package day11.task1;

public class Task1 {
    public static void main(String[] args) {
        Warehouse warehouse = new Warehouse();
        Worker worker2 = new Picker(warehouse);
        Worker worker1 = new Courier(warehouse);

        businessProcess(worker2);
        businessProcess(worker1);

        System.out.println(warehouse.getCountPickedOrders());
        System.out.println(warehouse.getCountDeliveredOrders());
        System.out.println(worker2);
        System.out.println(worker1);

        Warehouse warehouse2 = new Warehouse();
        Worker worker3 = new Picker(warehouse2);
        Worker worker4 = new Courier(warehouse2);

        worker3.doWork();
        worker4.doWork();


    }

    public static void businessProcess(Worker worker) {
        for (int i = 0; i < 10000; i++) {
            worker.doWork();
        }
        worker.bonus();
    }
}
