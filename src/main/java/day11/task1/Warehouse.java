package day11.task1;

public class Warehouse {
    int countPickedOrders;
    int countDeliveredOrders;

    @Override
    public String toString() {
        return "Склад: " +
                "Количество собранных заказов: " + countPickedOrders +
                "Количество доставленных заказов: " + countDeliveredOrders;
    }

    public int getCountPickedOrders() {
        return countPickedOrders;
    }

    public int getCountDeliveredOrders() {
        return countDeliveredOrders;
    }
}
