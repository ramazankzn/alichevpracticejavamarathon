package day11.task1;

public class Courier implements Worker {
    private int salary;
    private boolean isPayed;
    private Warehouse warehouse;


    @Override
    public String toString() {
        return "Courier{" +
                "salary=" + salary +
                ", isPayed=" + isPayed +
                '}';
    }

    public Courier(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public int getSalary() {
        return salary;
    }

    public boolean isPayed() {
        return isPayed;
    }

    @Override
    public void doWork() {
        salary += 100;
        warehouse.countDeliveredOrders++;

    }

    @Override
    public void bonus() {
        if (warehouse.getCountDeliveredOrders() >= 10000 && !isPayed) {
            salary += 50000;
            isPayed = true;
        } else if (isPayed) System.out.println("Бонус уже был выплачен");
        else if (warehouse.getCountPickedOrders() < 10000) System.out.println("Бонус пока не доступен");

    }
}
