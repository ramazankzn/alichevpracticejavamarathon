package day11.task2;

public class Warrior extends Hero implements PhysAttack {

    private int physAtt;

    public Warrior() {
        this.setHealth(100);
        this.setPhysDef(80);
        this.setMagicDef(0);
        this.physAtt = 30;
    }

    @Override
    public void physicalAttack(Hero hero) {
        hero.setHealth(hero.getHealth() - (physAtt - (physAtt * hero.getPhysDef() / 100)));
        if (hero.getHealth() < MIN_HEALTH) hero.setHealth(MIN_HEALTH);
    }

    @Override
    public String toString() {
        return "Warrior{" +
                "health=" + getHealth() +
                ", physDef=" + getPhysDef() +
                ", magicDef=" + getMagicDef() +
                ", physAtt=" + physAtt +
                '}';
    }
}
