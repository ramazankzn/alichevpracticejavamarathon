package day11.task2;

public class Shaman extends Hero implements Healer, PhysAttack, MagicAttack {

    private int physAtt;
    private int magicAtt;
    private static final int HEAL_HIMSELF = 25;
    private static final int HEAL_TEAMMATE = 25;

    public Shaman() {
        this.setHealth(100);
        this.setHealth(20);
        this.setHealth(20);
        this.physAtt = 10;
        this.magicAtt = 15;
    }

    @Override
    public void healHimself() {
        setHealth(getHealth() + HEAL_HIMSELF);
        if (getHealth() < MAX_HEALTH) setHealth(MAX_HEALTH);
    }

    @Override
    public void healTeammate(Hero hero) {
        hero.setHealth(hero.getHealth() + HEAL_TEAMMATE);
        if (hero.getHealth() > MAX_HEALTH) {
            hero.setHealth(MAX_HEALTH);
        }
    }

    @Override
    public void magicalAttack(Hero hero) {
        hero.setHealth(hero.getHealth() - (magicAtt - (magicAtt * hero.getMagicDef() / 100)));

        if (hero.getHealth() < MIN_HEALTH) hero.setHealth(MIN_HEALTH);
    }

    @Override
    public String toString() {
        return "Shaman{" +
                "health=" + getHealth() +
                ", physDef=" + getPhysDef() +
                ", magicDef=" + getMagicDef() +
                ", physAtt=" + physAtt +
                ", magicAtt=" + magicAtt +
                '}';
    }

    @Override
    public void physicalAttack(Hero hero) {
        hero.setHealth(hero.getHealth() - (physAtt - (physAtt * hero.getPhysDef() / 100)));
        if (hero.getHealth() < MIN_HEALTH) hero.setHealth(MIN_HEALTH);
    }
}
