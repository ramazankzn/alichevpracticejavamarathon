package day11.task2;

public class Paladin extends Hero implements Healer, PhysAttack {

    private int physAtt;
    private static final int HEAL_HIMSELF = 25;
    private static final int HEAL_TEAMMATE = 25;

    public Paladin() {
        this.setHealth(100);
        this.setPhysDef(50);
        this.setMagicDef(20);
        this.physAtt = 15;
    }

    @Override
    public void healHimself() {
        setHealth(getHealth() + HEAL_HIMSELF);
        if (getHealth() > MAX_HEALTH) setHealth(MAX_HEALTH);
    }

    @Override
    public void healTeammate(Hero hero) {
        hero.setHealth(getHealth() + HEAL_TEAMMATE);
        if (hero.getHealth() > MAX_HEALTH) hero.setHealth(MAX_HEALTH);

    }

    @Override
    public void physicalAttack(Hero hero) {
        hero.setHealth(hero.getHealth() - (physAtt - (physAtt * hero.getPhysDef() / 100)));
        if (hero.getHealth() < MIN_HEALTH) hero.setHealth(MIN_HEALTH);


    }

    @Override
    public String toString() {
        return "Paladin{" +
                "health=" + getHealth() +
                ", physDef=" + getPhysDef() +
                ", magicDef=" + getMagicDef() +
                ", physAtt=" + physAtt +
                '}';
    }
}
