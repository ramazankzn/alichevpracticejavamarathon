package day11.task2;

public abstract class Hero {
    private int health;
    private int physDef;

    public void setPhysDef(int physDef) {
        this.physDef = physDef;
    }

    public void setMagicDef(int magicDef) {
        this.magicDef = magicDef;
    }

    private int magicDef;
    static final int MAX_HEALTH = 100;
    static final int MIN_HEALTH = 0;

    public int getHealth() {
        return health;
    }

    public int getPhysDef() {
        return physDef;
    }

    public int getMagicDef() {
        return magicDef;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
