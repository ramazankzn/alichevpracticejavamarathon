package day11.task2;

public class Magician extends Hero implements MagicAttack {


    private int physAtt;
    private int magicAtt;

    public int getPhysAtt() {
        return physAtt;
    }

    public int getMagicAtt() {
        return magicAtt;
    }

    @Override
    public void magicalAttack(Hero hero) {
        hero.setHealth(hero.getHealth() - (magicAtt - (magicAtt * hero.getMagicDef() / 100)));

        if (hero.getHealth() < MIN_HEALTH) hero.setHealth(MIN_HEALTH);

    }

    public Magician() {
        this.setHealth(100);
        this.setPhysDef(0);
        this.setMagicDef(80);
        this.physAtt = 5;
        this.magicAtt = 20;
    }

    @Override
    public String toString() {
        return "Magician{" +
                "health=" + getHealth() +
                ", physDef=" + getPhysDef() +
                ", magicDef=" + getMagicDef() +
                ", physAtt=" + getPhysAtt() +
                ", magicAtt=" + getMagicAtt() +
                '}';
    }
}
